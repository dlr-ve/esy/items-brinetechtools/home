Welcome to BrineTechTools - Tools for the techno-economic modeling of desalination and wastewater treatment technologies.
The software tools are programmed in Python and have been used until now for the modeling of a variety of treatment plants - ranging from seawater desalination to coalmine effluents. These tools were developed within the ZERO BRINE project (ZERO BRINE – Industrial Desalination – Resource Recovery – Circular Economy), funded by the European Union’s Horizon 2020 research and innovation programme under Grant Agreement no. 730390.
More information can be found on the project [website](https://zerobrine.eu/).

## Aim
The BrineTechTools enable a techno-economic simulation of various thermal- and membrane-based water treatment technologies for the recovery of water and minerals. The tools include:
- Nanofiltration (NF)
- Reverse osmosis (RO)
- Electrodialysis (ED)
- Multi-effect distillation (MED)
- various crystallizer tools tailored to the salts/minerals being recovered

These tools can be used independently or integrated via a suitable integration platform to simulate entire treatment chains. As a simulation result, the user obtains the equipment sizing (e.g., the membrane area of an RO plant, the number of vessels in an NF plant, etc.), the energy demand (electrical and thermal, if applicable), the recoverable amount of pure water or minerals, and the investment and operating costs for the plant.

## Structure
Within the gitlab project, each tool has its individual Python script (the file name has a prefix 'main_'. For example, for nanofiltration: main_nanofiltration.py). At the bottom of the script, exemplary executable lines are provided. The parameter values for the positional arguments are user-defined. Each tool also requires some yaml files as input. These can be found in the 'input/{tool name}' folder.

A text file is available for documentation; a more detailed documentation was published within the ZERO BRINE project as [Deliverable D5.2](https://zerobrine.eu/wp-content/uploads/2022/02/D5.2-Software-Tools-for-the-Simulation-of-Selected-Brine-Treatment-Technologies.pdf).

## Enabling researchers
The BrineTechTools represent scientific prototypes developed for specific case studies and in a specific project context. The software tools are not intended for commercial use and their accuracy and usability for other applications is not guaranteed. The open source release is for the purpose of scientific exchange only. If you are interested in further development of these tools, please contact the DLR team.

The latest related publications can be found in our [research section](https://dlr-ve.gitlab.io/esy/items-brinetechtools/home/page/research/).

## Integration to treatment chains
To further enhance the capability of these tools, they can be integrated in desired configurations to simulate entire treatment chains. This requires a suitable integration platform. In the ZERO BRINE project, the open source integration platform [RCE](https://rcenvironment.de/) developed at DLR was used for this purpose.
Please contact us if you would like to model a specific treatment chain or case study.

## Getting started
| Content | URL |
| --------: | :---- |
| BrineTechTools gitlab project | https://gitlab.com/dlr-ve/esy/items-brinetechtools/brinetechtools |


## Contact
Feel free to get in [contact](mailto:brinetechtools@dlr.de) or contribute to [brinetechtools](https://gitlab.com/dlr-ve/esy/items-brinetechtools/brinetechtools).

![](DLR_bottom_banner.jpg )