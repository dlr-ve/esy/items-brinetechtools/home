---
title: Current research
subtitle: 
date: 2022-06-08T00:22:14-04:00
comments: false
---

## Project
ZERO BRINE: ZERO BRINE – Industrial Desalination – Resource Recovery – Circular Economy. [Link](https://zerobrine.eu/). This project has received funding from the European Union’s Horizon 2020 Research and Innovation Programme under grant agreement No 730390.

## Publications
Micari, M.; Cipollina, A.; Tamburini, A.; Moser, M.; Bertsch, V.; Micale, G. (2019): Combined membrane and thermal desalination processes for the treatment of ion exchange resins spent brine. In: Applied Energy 254, S. 113699. DOI: [10.1016/j.apenergy.2019.113699](https://doi.org/10.1016/j.apenergy.2019.113699).

Micari, M.; Cipollina, A.; Tamburini, A.; Moser, M.; Bertsch, V.; Micale, G. (2020): Techno-economic analysis of integrated processes for the treatment and valorisation of neutral coal mine effluents. In: Journal of Cleaner Production 270, S. 122472. DOI: [10.1016/j.jclepro.2020.122472](https://doi.org/10.1016/j.jclepro.2020.122472).

Micari, M.; Moser, M.; Cipollina, A.; Fuchs, B.; Ortega-Delgado, B.; Tamburini, A.; Micale, G. (2019): Techno-economic assessment of multi-effect distillation process for the treatment and recycling of ion exchange resin spent brines. In: Desalination 456, S. 38-52. DOI: [10.1016/j.desal.2019.01.011](https://doi.org/10.1016/j.desal.2019.01.011).

Micari, M.; Moser, M.; Cipollina, A.; Tamburini, A.; Micale, G.; Bertsch, V. (2020): Towards the implementation of circular economy in the water softening industry: A technical, economic and environmental analysis. In: Journal of Cleaner Production 255, S. 120291. DOI: [10.1016/j.jclepro.2020.120291](https://doi.org/10.1016/j.jclepro.2020.120291).

Pawar, Nikhil Dilip; Harris, Steve; Mitko, Krzysztof; Korevaar, Gijsbert (2022): Valorization of coal mine effluents - Challenges and economic opportunities. In: Water Resources and Industry, S. 100179. DOI: [10.1016/j.wri.2022.100179](https://doi.org/10.1016/j.wri.2022.100179).

Micari, Marina; Diamantidou, Dionysia; Heijman, Bas; Moser, Massimo; Haidari, Amir; Spanjers, Henri; Bertsch, Valentin (2020): Experimental and Theoretical Characterization of Commercial Nanofiltration Membranes for the Treatment of Ion Exchange Spent Regenerant. In: Journal of Membrane Science 606, S. 118117. DOI: [10.1016/j.memsci.2020.118117](https://doi.org/10.1016/j.memsci.2020.118117s).

Micari, Marina (2020): Integration of Desalination and Purification Processes for the Treatment and Valorisation of Industrial Brines. Institute for Building Energetics, Thermotechnology and Energy Storage (IGTE), University of Stuttgart. Dissertation/doctoral thesis, 10.18419/opus-11103.