---
title: Privacy
subtitle: 
date: 2022-06-08T00:22:14-04:00
comments: false
---

This website is deployed using [GitLab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).

Therefore, please refer to [GitLab's privacy policy](https://about.gitlab.com/privacy/).
