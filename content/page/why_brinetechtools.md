---
title: Why BrineTechTools?
subtitle: 
date: 2022-06-08T00:22:14-04:00
comments: false
---

Being based on Python and open-source, BrineTechTools are accessible and usable by everyone without the need for commercial software. Moreover, these tools provide the following benefits:
- the techno-economic modeling of each technology is validated against literature / experiments and well documented in the form of research articles
- the user has full control over the parameter selection (both technical and economic), which allows for application-specific customization
- using a suitable integration platform, the tools can be integrated to simulate entire wastewater treatment plants

Moreover, users from academia and industry have the freedom to further refine the mathematical formulations in the code as needed. This can help to increase the robustness of the model in a variety of scenarios.